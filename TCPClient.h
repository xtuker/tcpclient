#pragma once
#include <WinSock2.h>
#include <string>

class TCPClient
{
private:
    SOCKET sockd;
    WSAData wsa;
    static const size_t SizeOfBuffer = 65000;

    void Log(std::string const &msg){}

protected:
    sockaddr_in GetAddress(std::string const & addr, unsigned short port);
public:
    TCPClient(void);
    ~TCPClient(void);

    bool Connect(std::string const &addr, unsigned short port);
    void Disconnect();

    size_t Send(std::string const &data);
    std::string Receive();
    std::string Receive(size_t count);
};

